import React, { Component } from "react";

// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

//
// import Posts from "./components/Posts";
// import ShowPost from "./components/ShowPost";
// import ShowPPost from "./components/ShowPPost";
// import MyPosts from "./components/MyPosts";
// import AddPost from "./components/AddPost";
// import Home from "./components/Home";
// import Login from "./components/Login";
// import Logout from "./components/Logout";
// import ForgotPwd from "./components/ForgotPwd";
// import ResetPwd from "./components/ResetPwd";
//
// import Register from "./components/Register";
//
//
// import Register from "./components/Register";


import Menu from "./components/templates/Menu";
import Footer from "./components/templates/Footer";

import AdDisplay from "./components/templates/AdDisplay";
import BuyCar from "./components/templates/BuyCar";


import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/Contact";

import Login from "./components/auth/Login";
import Logout from "./components/auth/Logout";
import Register from "./components/auth/Register";
import ForgotPassword from "./components/auth/ForgotPassword";
import ResetPassword from "./components/auth/ResetPassword";


import PostAd from "./components/admin/PostAd";
import UploadAdImages from "./components/admin/UploadAdImages";
import UploadMultipleAdImages from "./components/admin/UploadMultipleAdImages";
import UploadIndividualAdImages from "./components/admin/UploadIndividualAdImages";

import { store } from "./helpers";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";


class App extends Component {
  render() {
    return (


            <Provider store={store}>
              <BrowserRouter>

              <div className="App">
                <Menu />


                  <div className="auth-wrapper">
                    <div className="auth-inner">
                      <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/contact" component={Contact} />

                        <Route exact path="/login" component={Login} />
                        <Route exact path="/logout" component={Logout} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/fgt-pwd" component={ForgotPassword} />
                        <Route exact path="/reset-pwd" component={ResetPassword} />

                        <Route exact path="/buy-car" component={BuyCar} />
                        <Route exact path="/display-ad" component={AdDisplay} />

                        <Route exact path="/post-ad" component={PostAd} />
                        <Route exact path="/upload-ad-images" component={UploadAdImages} />
                        <Route exact path="/u-m-i" component={UploadMultipleAdImages} />
                        <Route exact path="/u-i-i" component={UploadIndividualAdImages} />



                      </Switch>
                    </div>
                  </div>
                <Footer />
              </div>


              </BrowserRouter>
            </Provider>


    );
  }
}

export default App;
