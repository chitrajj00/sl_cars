import { combineReducers } from "redux";
// import userReducer from "./user-reducer";
// import authReducer from "./auth-reducer";
// import postsReducer from "./posts-reducer";
import alertReducer from "./alert-reducer";
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  alert: alertReducer,
  form:  formReducer
});
