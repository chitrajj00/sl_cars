import React, { Component } from "react";

import { Link } from "react-router-dom";

export default class AdPrice extends Component {
    render() {
        return (
          <div className="row display-ad-contact">
            <h5>Contact US for More details</h5>
            <p>Date Posted: 21/04/2020</p>
            <p>Contact Srilaxmi Cars</p>
            <div>
              <Link to="/contact" className="btn btn-primary">Find Out More!</Link>
            </div>
          </div>
        );
    }
}
