// AdTitle.js
// AdImages.js
// AdContact.js
// AdDescription.js

import React, { Component } from "react";

import { Link } from "react-router-dom";

// import AdImages from "./AdImages";
import AdImages from "./AdImagess";
import AdPrice from "./AdPrice";
import AdSellerContact from "./AdSellerContact";
import AdDescription from "./AdDescription";
import MiniAd from "./MiniAd";
export default class AdDisplay extends Component {
    render() {
        return (

          <div className="container">
            <br></br><br></br>
            <div className="row">
              <div className="col-md-8 display-ad-images">
                <AdImages />
              </div>
              <div className="col-md-4 display-ad-price-contact">
                <AdPrice />
                <br></br>
                <AdSellerContact />
              </div>
            </div>
            <div className="row display-ad-description">
              <div className="col-md-8 display-ad-description">
                <AdDescription />
              </div>
            </div>
            <br></br>
            <br></br>
            <hr />
            <h3>Similar posts</h3>
            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>

            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>
            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>
            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>
            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>

          </div>

        );
    }
}
