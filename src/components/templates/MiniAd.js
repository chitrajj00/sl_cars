import React, { Component } from "react";

import { Link } from "react-router-dom";

export default class MiniAd extends Component {
    render() {
        return (
          <div className="col-lg-3 col-md-6 mb-4">
            <div className="card h-100 bg-light">
              <img className="card-img-top" src="https://i.ibb.co/L1jMkcf/car.jpg" alt="" />
              <div className="card-body">
              <div className="mini-ad-price"><strong>₹ 12,35,000</strong></div>
                <div className="mini-ad-model">Mahindra Xuv500</div>
                <div className="mini-ad-fuel"><span className="mini-ad-yr"><strong>2011</strong></span>&nbsp;&nbsp;&nbsp;Diesel  12,000 Km</div>
              </div>
              <div className="card-footer">
                <div className="mini-ad-model"><small className="text-muted">Ad posted Feb 22</small>
                <Link to="/display-ad" className="btn btn-outline-primary btn-sm mini-ad-view">view</Link>
                </div>

              </div>
            </div>
          </div>
        );
    }
}
