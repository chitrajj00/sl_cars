import React, { Component } from "react";

// import { Link } from "react-router-dom";

// src="https://i.ibb.co/g3dt6mJ/car41.jpg"
// <a href="https://ibb.co/x6DTb7K"><img src="https://i.ibb.co/3c7ZbC5/car4.jpg" alt="car4" border="0"></a>
import Carousel from 'react-bootstrap/Carousel';

export default class AdImages extends Component {
    render() {
        return (
          <div>
              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block not-good-img"
                    src="https://i.ibb.co/4fTws5Y/car1.jpg"
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block not-good-img"
                    src="https://i.ibb.co/Qn2hNgS/car.jpg"
                    alt="Third slide"
                  />

                  <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block not-good-img"
                    src="https://i.ibb.co/3c7ZbC5/car4.jpg"
                    alt="Third slide"
                  />

                  <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </div>
        );
    }
}
