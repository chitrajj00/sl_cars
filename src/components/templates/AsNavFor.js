import React, { Component } from "react";
import Slider from "react-slick";

// import "~slick-carousel/slick/slick.css";
// import "~slick-carousel/slick/slick-theme.css";

import '../../../node_modules/slick-carousel/slick/slick.css';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';

import './slickk.css'

const data = [
  {
    imageUrl: "https://i.ibb.co/4fTws5Y/car1.jpg",
    carorselHeader: "First slide label",
    carorselSubHeader:
      "Nulla vitae elit libero, a pharetra augue mollis interdum."
  },
  {
    imageUrl: "https://i.ibb.co/Qn2hNgS/car.jpg",
    carorselHeader: "First slide label",
    carorselSubHeader:
      "Nulla vitae elit libero, a pharetra augue mollis interdum."
  },
  {
    imageUrl: "https://i.ibb.co/3c7ZbC5/car4.jpg",
    carorselHeader: "First slide label",
    carorselSubHeader:
      "Nulla vitae elit libero, a pharetra augue mollis interdum."
  },
  {
    imageUrl: "https://i.ibb.co/Qn2hNgS/car.jpg",
    carorselHeader: "First slide label",
    carorselSubHeader:
      "Nulla vitae elit libero, a pharetra augue mollis interdum."
  }
];

export default class AsNavFor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  render() {
    return (
      <div style={{ maxWidth: "600px", margin: "40px", textAlign: "center" }}>
        <h2>Slider Syncing (AsNavFor)</h2>
        <Slider
          asNavFor={this.state.nav2}
          ref={slider => (this.slider1 = slider)}
        >
          {data.map(d => (
            <div>
              <img src={d.imageUrl} style={{ maxHeight: "400px" }} alt=""></img>
            </div>
          ))}
        </Slider>
        <Slider
          asNavFor={this.state.nav1}
          ref={slider => (this.slider2 = slider)}
          slidesToShow={3}
          swipeToSlide={true}
          focusOnSelect={true}
        >
          {data.map(d => (
            <div
              style={{
                maxWidth: "100px",
                maxHeight: "100px",
                verticalAlign: "middle"
              }}
            >
              <img
                src={d.imageUrl}
                style={{
                  maxHeight: "100px"
                }}
                alt=""
              ></img>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}
