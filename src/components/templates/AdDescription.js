import React, { Component } from "react";

import { Link } from "react-router-dom";

export default class AdImages extends Component {
    render() {
        return (
          <div>
          <p>
            Details <br></br>
      Brand
      Toyota
      Model
      Innova
      Year
      2014.0
      Fuel
      Diesel
      KM driven
      140,000 km
      Description
      well maintained

      ADDITIONAL VEHICLE INFORMATION:

      Registration Transfer: Yes<br></br>

      Color: Silver<br></br>

      Air Conditioning: Automatic Climate Control<br></br>

      Aux Compatibility: Yes<br></br>

      USB Compatibility: Yes<br></br>

      Service History: Available<br></br>

      No. of Owners: First<br></br>

      Registration Place: TS<br></br>

      AM/FM Radio: Yes<br></br>

      Accidental: No<br></br>

      Flood Affected: No<br></br>

      Parking Sensors: Yes<br></br>

      Transmission: Manual<br></br>

      Power Windows: Front & rear<br></br>

      Variant: Innova 2.5 GX 8 STR BS-IV<br></br>

      Vehicle Certified: No<br></br>

      Engine Capacity/Displacement (in Cc): 2500.0<br></br>

      Fog Lamps: Yes<br></br>

      Display Screen: Yes<br></br>

      Finance: Yes<br></br>

      Exchange: Yes<br></br>

      Type of Car: SUV<br></br>

      Battery Condition: New<br></br>

      Insurance Type: No Insurance<br></br>

      Condition: Used<br></br>

      ABS: Yes<br></br>

      Power steering: Yes<br></br>

      Adjustable Steering: Yes<br></br>

      Make Month: May<br></br>

      Bluetooth: Yes<br></br>

      Alloy Wheels: Yes<br></br>

      Lock System: Remote Controlled Central<br></br>

      Tyre Condition: New<br></br>

      Number of Airbags: 2 airbags<br></br>
          </p>
          </div>

        );
    }
}
