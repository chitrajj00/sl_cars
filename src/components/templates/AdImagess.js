import React, { Component } from "react";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";

const images = [
  "https://i.ibb.co/4fTws5Y/car1.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg",
  "https://i.ibb.co/3c7ZbC5/car4.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg",
  "https://i.ibb.co/4fTws5Y/car1.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg",
  "https://i.ibb.co/3c7ZbC5/car4.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg",
  "https://i.ibb.co/4fTws5Y/car1.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg",
  "https://i.ibb.co/3c7ZbC5/car4.jpg",
  "https://i.ibb.co/Qn2hNgS/car.jpg"
];

export default class AdImagess extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false
    };
  }

  render() {
    const { photoIndex, isOpen } = this.state;

    return (
      <div>
        {images.map((currElement, index) => (
          <button
            onClick={() => {
              this.setState({ isOpen: true, photoIndex: index });
            }}
          >
            <img
              src={currElement}
              style={{ height: "180px", width: "180px", margin: "4px" }}
              alt=""
            ></img>
          </button>
        ))}

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })
            }
          />
        )}
      </div>
    );
  }
}
