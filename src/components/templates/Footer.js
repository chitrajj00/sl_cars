import React, { Component } from "react";


export default class Footer extends Component {
    render() {
        return (
          <footer className="py-3 bg-dark">
              <p className="m-0 text-center text-white">Copyright &copy; srilaxmicars.com 2020</p>
          </footer>
        );
    }
}
