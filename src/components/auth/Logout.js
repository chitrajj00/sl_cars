import React, { Component } from "react";
import * as actions from "../../actions";
import { connect } from 'react-redux';


class Logout extends Component {

    componentDidMount() {
      this.props.logoutSubmit();
    }

    render() {
        return (
          <div>
              <h2>Sorry to see you go!!!</h2>
          </div>
        );
    }
}

export default connect(null, actions)(Logout);
