import React, { Component } from "react";

import { connect } from 'react-redux';

import * as actions from "../../actions";
import { Field, reduxForm } from 'redux-form';
import { compose } from 'redux';

import { Link } from "react-router-dom";


class Login extends Component {

    renderField (field) {
      const { meta: { touched, error } } = field;
      const className= `form-group ${touched && error ? 'has-danger': ''}`;

      return(
        <div className={className}>
            <label>{field.label}</label>
            <input
              className="form-control"
              placeholder={field.placeholder}
              type={field.type}
              {...field.input}
            />
              <div className="text-help">
                {touched ? error : ''}
            </div>
      </div>
      );
    }


    onSubmit(values) {
      this.props.loginSubmit(values, () => {
        this.props.history.push("/");
      });
    };

    render() {
        const { handleSubmit } = this.props;
        return (

          <div class="container">
          <br></br>
          <br></br>
          <br></br>
            <div class="row">
              <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                  <div class="card-body">
                    <h5 class="card-title text-center">Login</h5>
                    <form class="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                        <Field
                          label="Email address"
                          placeholder="Email address"
                          name="email"
                          type="email"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                        <Field
                          label="Password"
                          placeholder="Password"
                          name="password"
                          type="password"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                        <div class="custom-control custom-checkbox mb-3">
                          <input type="checkbox" class="custom-control-input" id="customCheck1" />
                          <label class="custom-control-label" htmlFor="customCheck1">Remember password</label>
                        </div>
                      <button type="submit" className="btn btn-primary btn-block">Login</button>
                      <div class="text-center">
                        <Link to="/fgt-pwd" className="small">Forgot password?</Link>
                      </div>
                      <div class="text-center">
                          <Link to="/register" className="small">First time user? Register here</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>

          </div>

        );
    }
}



function validate(values){
	const errors = {};

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.password) {
    errors.password = 'Required';
  }

  return errors
}



const mapStateToProps = state => {
  return { errorMessage: state };
};


export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
  	validate,     // validate: validate
  	form:'LoginForm'
  }))(Login)
