import React, { Component } from "react";

import { connect } from 'react-redux';

import * as actions from "../../actions";
import { Field, reduxForm } from 'redux-form';
import { compose } from 'redux';

import { Link } from "react-router-dom";


class Register extends Component {

    renderField (field) {
      const { meta: { touched, error } } = field;
      const className= `form-group ${touched && error ? 'has-danger': ''}`;

      return(
        <div className={className}>
            <label>{field.label}</label>
            <input
              className="form-control"
              placeholder={field.placeholder}
              type={field.type}
              {...field.input}
            />
              <div className="text-help">
                {touched ? error : ''}
            </div>
      </div>
      );
    }


    onSubmit(values) {
      this.props.loginSubmit(values, () => {
        this.props.history.push("/");
      });
    };

    render() {
        const { handleSubmit } = this.props;
        return (

          <div class="container">
          <br></br>
          <br></br>
          <br></br>
            <div class="row">
              <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                  <div class="card-body">
                    <h5 class="card-title text-center">Register</h5>
                    <form class="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                      <Field
                        label="Name"
                        name="name"
                        placeholder="Name"
                        type="text"
                        autoComplete="none"
                        component={this.renderField}
                      />
                      <Field
                        label="Email address"
                        placeholder="Email address"
                        name="email"
                        type="email"
                        autoComplete="none"
                        component={this.renderField}
                      />
                      <Field
                        label="Password"
                        placeholder="Password"
                        name="password"
                        type="password"
                        autoComplete="none"
                        component={this.renderField}
                      />
                      <Field
                        label="Re enter Password"
                        placeholder="Re enter Password"
                        name="password_confirmation"
                        type="password"
                        autoComplete="none"
                        component={this.renderField}
                      />
                      <button type="submit" className="btn btn-primary btn-block">Register</button>
                      <div class="text-center">
                          <Link to="/login" className="small">Already registered? Login here</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <br></br>
          </div>

        );
    }
}



function validate(values){
	const errors = {};

  if (!values.name) {
    errors.name = 'Required'
  }
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.password_confirmation ) {
    errors.password_confirmation = 'Required' ;
  } else if (values.password_confirmation !== values.password) {
    errors.password_confirmation = 'Password mismatched' ;
  }
  return errors
}




const mapStateToProps = state => {
  return { errorMessage: state };
};


export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
  	validate,     // validate: validate
  	form:'RegisterForm'
  }))(Register)
