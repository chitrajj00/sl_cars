import React, { Component } from "react";

import { connect } from 'react-redux';

import * as actions from "../../actions";
import { Field, reduxForm } from 'redux-form';
import { compose } from 'redux';

import { Link } from "react-router-dom";


class PostAd extends Component {

    renderField (field) {
      const { meta: { touched, error } } = field;
      const className= `form-group ${touched && error ? 'has-danger': ''}`;

      return(
        <div className={className}>
            <label>{field.label}</label>
            <input
              className="form-control"
              type={field.type}
              placeholder={field.placeholder}
              {...field.input}
            />
              <div className="text-help">
                {touched ? error : ''}
            </div>
      </div>
      );
    }


    onSubmit(values) {
      this.props.loginSubmit(values, () => {
        this.props.history.push("/");
      });
    };

    render() {
        const { handleSubmit } = this.props;
        return (

          <div className="container">
          <br></br>
          <br></br>
          <br></br>
          <div className="row">
            <div className="col-lg-9 col-md-7 col-lg-5 mx-auto">
              <div className="card card-signin my-5">
                <div className="card-body">
                  <h4 className="card-title text-left">Add Car Details</h4>
                  <hr />
                    <form className="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <Field
                          label="Brand"
                          placeholder="Brand"
                          name="brand"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <Field
                          label="Model"
                          placeholder="Model"
                          name="model"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <Field
                          label="Year"
                          placeholder="Year"
                          name="year"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <Field
                          label="Km Driven"
                          placeholder="Km Driven"
                          name="km_driven"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <label>Fuel Type</label>
                        <select className="form-control" name="fuel_type">
                         <option hidden>Fuel Type</option>
                         <option value="AL">Petrol</option>
                         <option value="AK">Diesel</option>
                         <option value="AZ">LPG</option>
                         <option value="AR">Electric</option>
                         <option value="CA">CNG & HYBRID</option>
                        </select>
                      </div>
                      <div className="form-group col-md-4">
                      <label >Transmission Type</label>
                      <select className="form-control" name="transmission_type">
                       <option hidden>Transmission Type</option>
                       <option value="manual">Manual</option>
                       <option value="auto">Auto</option>
                      </select>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <Field
                          placeholder="Color"
                          label="Color"
                          name="color"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <label >Condition</label>
                        <select className="form-control" name="fuel_type">
                         <option hidden>Condition</option>
                         <option value="good">Good</option>
                         <option value="fair">Fair</option>
                         <option value="excellent">Excellent</option>
                        </select>
                      </div>
                      <div className="form-group col-md-4">
                      <label >No of Owners</label>
                      <select className="form-control" name="no_of_owners">
                       <option hidden>No of Owners</option>
                       <option value="1">1</option>
                       <option value="2">2</option>
                       <option value="3">3</option>
                       <option value="4">4</option>
                       <option value="5">5</option>
                       <option value="6">6</option>
                      </select>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <Field
                          placeholder="Car Type"
                          label="Car Type"
                          name="car_type"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                        <Field
                          placeholder="Interior Color"
                          label="Interior Color"
                          name="interior_color"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                      <div className="form-group col-md-4">
                      <label >Power Steering</label>
                      <select className="form-control" name="power_steering" placeholder="Power Steering">
                       <option hidden>Power Steering</option>
                       <option value="true">Yes</option>
                       <option value="false">No</option>
                      </select>
                      </div>
                    </div>
                    <hr />
                    <h4 className="card-title text-left">Car Price Details and additional Information</h4>
                    <hr />

                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <Field
                          label="Display Price"
                          placeholder="Display Price"
                          name="display_name"
                          type="text"
                          autoComplete="none"
                          component={this.renderField}
                          autoFocus
                        />
                      </div>
                    </div>

                    <div className="form-row">
                      <label>Additional Information about car</label>
                      <textarea className="form-control" name="additional_info" rows="3" placeholder="Add extra Car Information here to display..."></textarea>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <div className="text-center mt-4">
                          <button className="btn btn-lg btn-primary btn-login text-uppercase font-weight-bold mb-2" type="submit">Back</button>
                        </div>
                      </div>
                      <div className="form-group col-md-4">
                      </div>
                      <div className="form-group col-md-4">
                        <div className="text-center mt-4">
                          <button className="btn btn-lg btn-primary btn-login text-uppercase font-weight-bold mb-2" type="submit">Next</button>
                        </div>
                      </div>
                    </div>


                    </form>
                  </div>
                </div>
              </div>
            </div>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>

          </div>

        );
    }
}



function validate(values){
	const errors = {};

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.password) {
    errors.password = 'Required';
  }

  return errors
}



const mapStateToProps = state => {
  return { errorMessage: state };
};


export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
  	validate,     // validate: validate
  	form:'PostAdForm'
  }))(PostAd)
