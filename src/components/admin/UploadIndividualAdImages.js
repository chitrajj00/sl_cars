import React, { Component } from 'react';

export default class UploadIndividualAdImages extends Component {

    fileObj = [];
    fileArray = [];

    constructor(props) {
        super(props)
        this.state = {
            file: [null]
        }
        this.uploadMultipleFiles = this.uploadMultipleFiles.bind(this)
        this.uploadFiles = this.uploadFiles.bind(this)
    }

    uploadMultipleFiles(e) {
        console.log(e.target.files);
        this.fileObj.push(e.target.files)
        for (let i = 0; i < this.fileObj[0].length; i++) {

            this.fileArray.push(URL.createObjectURL(this.fileObj[0][i]))
        }
        this.setState({ file: this.fileArray })
    }

    uploadFiles(e) {
        e.preventDefault()
        console.log(this.state.file)
    }

    render() {
        return (

          <div className="container">
          <br></br>
          <br></br>
          <br></br>
          <div className="row">
            <div className="col-lg-9 col-md-7 col-lg-5 mx-auto">
              <div className="card card-signin my-5">
                <div className="card-body">
                  <h4 className="card-title text-left">Upload Individual Car Images</h4>

                  <hr />
                  <form>
                      <div className="form-group multi-preview">
                          {(this.fileArray || []).map(url => (
                              <img src={url} alt="..." width="200" height="200" />
                          ))}
                      </div>
                      <br />
                      <br />
                      <br />
                      <div className="form-group">
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                          <input type="file" className="form-control" onChange={this.uploadMultipleFiles} />
                      </div>
                      <button type="button" className="btn btn-danger btn-block" onClick={this.uploadFiles}>Upload</button>
                  </form >
                  <br />
                  <br />
                  </div>
                </div>
              </div>
            </div>

          </div>



        )
    }
}
