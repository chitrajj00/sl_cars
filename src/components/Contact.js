import React, { Component } from "react";


export default class Contact extends Component {
    render() {
        const styleH = {height : '400px'};
        return (
          <div className="container">
            <br></br>
            <section className="section pb-5">

              <h2 className="section-heading h1 pt-4">Contact us</h2>
              <p className="section-description pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error
                amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam eum porro a
                pariatur accusamus veniam.</p>

              <div className="row">

                <div className="col-lg-5 mb-4">

                  <div className="card">

                    <div className="card-body">
                      <div className="form-header blue accent-1">
                        <h3><i className="fas fa-envelope"></i> Write to us:</h3>
                      </div>

                      <p>We'll write rarely, but with only the best content.</p>
                      <br></br>

                      <div className="md-form">
                        <label htmlFor="form-name">Your name</label>
                        <i className="fas fa-user prefix grey-text"></i>
                        <input type="text" id="form-name" className="form-control" />
                      </div>

                      <div className="md-form">
                        <label htmlFor="form-email">Your email</label>
                        <i className="fas fa-envelope prefix grey-text"></i>
                        <input type="text" id="form-email" className="form-control" />
                      </div>

                      <div className="md-form">
                        <label htmlFor="form-Subject">Subject</label>
                        <i className="fas fa-tag prefix grey-text"></i>
                        <input type="text" id="form-Subject" className="form-control" />
                      </div>

                      <div className="md-form">
                        <label htmlFor="form-text">Message</label>
                        <i className="fas fa-pencil-alt prefix grey-text"></i>
                        <textarea id="form-text" className="form-control md-textarea" rows="3"></textarea>
                      </div>

                      <div className="text-center mt-4">
                        <button className="btn btn-primary">Submit</button>
                      </div>

                    </div>

                  </div>

                </div>


                <div className="col-lg-7">

                  <div id="map-container-google-11" className="z-depth-1-half map-container-6" style={{height: '400px'}}>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7612.327042786825!2d78.56843023378924!3d17.45188582467191!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9d503e7573a9%3A0xaa27153f4794004c!2sSri%20Laxmi%20Cars!5e0!3m2!1sen!2sus!4v1587166387214!5m2!1sen!2sus" frameBorder="0" style={{border: '0'}} allowFullScreen></iframe>
                  </div>
                  <br></br>
                  <div className="row text-center">
                    <div className="col-md-4">
                      <a className="btn-floating blue accent-1"><i className="fas fa-map-marker-alt"></i></a>
                      <p>Secunderabad, TG-500040</p>
                      <p>India</p>
                    </div>

                    <div className="col-md-4">
                      <a className="btn-floating blue accent-1"><i className="fas fa-phone"></i></a>
                      <p>+ 91 9908 109 814</p>
                      <p>Mon-Sat, 10:00AM - 8:00PM</p>
                    </div>

                    <div className="col-md-4">
                      <a className="btn-floating blue accent-1"><i className="fas fa-envelope"></i></a>
                      <p>ckumar.slc@gmail.com</p>
                      <p>ckumar.slc@gmail.com</p>
                    </div>
                  </div>

                </div>

              </div>

            </section>
          </div>
        );
    }
}
