import React, { Component } from "react";

import { Link } from "react-router-dom";


import HomeHeader from "./templates/HomeHeader";
import MiniAd from "./templates/MiniAd";
export default class Home extends Component {
    render() {
        return (

          <div className="container">
          <HomeHeader />


            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>

            <div className="row text-center">
              <MiniAd />
              <MiniAd />
              <MiniAd />
              <MiniAd />
            </div>

          </div>

        );
    }
}
