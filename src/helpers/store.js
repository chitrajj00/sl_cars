import thunk from "redux-thunk";
// import logger from "redux-logger";
import { compose, applyMiddleware, createStore } from "redux";

// import async from "../middlewares/async";
import rootReducer from "../reducers";
const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;
/**
 * composeEnhancers redux devtools
 */
const enhancer = composeEnhancers(
  // applyMiddleware(async),
  applyMiddleware(thunk)
  // , applyMiddleware(logger)
);
const store = createStore(rootReducer,
                          // {
                          //   auth: {
                          //           authenticated: {token: localStorage.getItem("jwtToken"),
                          //                           name:   localStorage.getItem("userName")}
                          //         }
                          // },
                          enhancer);
export default store;
